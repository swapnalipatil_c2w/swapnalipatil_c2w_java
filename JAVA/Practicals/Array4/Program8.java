

import java.util.*;

class Demo8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();
		char arr[]=new char[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.next().charAt(0);
		}
		int cnt=0;
		System.out.print("Enter character: ");
		char ch = sc.next().charAt(0);
		for(int j=0;j<size;j++){
			if(arr[j]==ch){
				cnt++;
			}

		}
		System.out.print(ch + " occurs " + cnt + " times in array ");

		


	}
}
