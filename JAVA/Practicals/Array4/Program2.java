

import java.util.*;

class Demo2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int min=arr[0];
		int max=arr[0];
		for(int j=0;j<size;j++){
			if(arr[j]<min){
				min=arr[j];
			}
			if(arr[j]>max){
				max=arr[j];
			}
		}
		System.out.println("Max is : " + max + " ");
		System.out.println("Min is : " + min + " ");
		System.out.println("Difference is : "+ (max-min) + " ");


	}
}
