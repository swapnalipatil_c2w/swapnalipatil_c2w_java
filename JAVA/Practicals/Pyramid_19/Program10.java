

import java.util.*;

class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		
		int ch = 64+rows;
		for(int i=1;i<=rows;i++){
			for(int sp=rows;sp>i;sp--){
				System.out.print(" ");
			}
				
			for(int j=1;j<=i*2-1;j++){
			
				if(j<i){
					System.out.print((char)ch);
					ch++;
				}
				else{
					System.out.print((char)ch);
					ch--;
				}

			}
			System.out.println();
			
			
		}
	}
}

