

import java.util.*;

class Demo2{

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter you rows: ");
		int rows = sc.nextInt();
		int num=1;
		for(int i=1;i<=rows;i++){
			
			for(int sp=rows;sp>i;sp--){
				System.out.print(" ");
			}
			for(int j=1;j<=i*2-1;j++){
				if(j<i){
					System.out.print(num);
				}
				else{
					System.out.print(num);
				}
				num++;

			}
			System.out.println();
		}
	}
}
