

import java.util.Scanner;

class Demo{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the number of rows: ");
        int rows = scanner.nextInt();

        for (int i = 1; i <= rows; i++) {
            for (int j = 1; j <= rows - i + 1; j++) {
                if (i % 2 == 0) {
                    char ch = (char) ('A' + rows - j);
                    System.out.print(ch + " ");
                } else {
                    System.out.print(j + " ");
                }
            }
            System.out.println();
        }

        scanner.close();
    }
}
