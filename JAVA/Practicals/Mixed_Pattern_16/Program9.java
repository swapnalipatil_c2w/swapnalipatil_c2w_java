

// 4 3 2 1
// C B A
// 2 1 
// A

import java.io.*;

class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		
		for(int i=1;i<=rows;i++){
			int ch=64+rows-i+1;

			int num=1;
			
			for(int j=rows;j>=i;j--){
					
				if(i%2==1){
					System.out.print(num + " ");
				}
				else{
					System.out.print((char)(ch)+ " ");
					ch--;	
				}
				
					
				num++;
			}
			
			
			System.out.println();
		}
	}
}

