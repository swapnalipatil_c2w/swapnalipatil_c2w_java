

import java.io.*;

class Demo4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		int incre=rows;
		for(int i=1;i<=rows;i++){
			int num=(rows+1)-i;
			for(int j=1;j<=i;j++){
				System.out.print(num + " ");
				num=num+incre;
			}
			incre--;
			System.out.println();
		}
	}
}
