

import java.io.*;

class Demo2{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		int ch = 64+rows;
		for(int i=1;i<=rows;i++){
			int num= rows-1+i;
			for(int j=1;j<=rows;j++){
				System.out.print( (char)(ch));
				System.out.print(num + " ");
				num--;
			}
			System.out.println();
		}
	}
}
