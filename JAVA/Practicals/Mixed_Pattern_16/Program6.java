

import java.io.*;

class Demo6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int ch = 96+rows;
			int num=rows;
			for(int j=1;j<=i;j++){
				if(i%2==1){
					System.out.print((char)(ch) + " ");
					ch--;
				}else{
					System.out.print(num + " ");
					num--;
				}
			}
			System.out.println();
		}
	}
}
			
			
				
