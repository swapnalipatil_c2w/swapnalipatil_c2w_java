

import java.util.*;

class Demo3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		int space=0;
		int col=0;
		
		for(int i=1;i<rows*2;i++){
			
			if(i<=rows){
				space=rows-i;
				col=i*2-1;
			}
			else{
				space = i-rows;
				col=col-2;
			}
			int num=1;
			
			for(int sp=1;sp<=space;sp++){
				System.out.print("\t");
			}
			for(int j=1;j<=col;j++){
				if(col<=1){
				System.out.print(num + "\t");
				num++;
				}
				else{
					System.out.print(num + "\t");
					num--;
				}
			
			}
			
			System.out.println();
		}
	}
}
