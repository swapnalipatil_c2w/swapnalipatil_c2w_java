

import java.util.*;

class Demo8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array1: ");
		int size=sc.nextInt();
		char arr[] = new char[size];
		
		for(int i=0;i<size;i++){
			arr[i]=sc.next().charAt(0);
		}
		
		for(int j=size-1;j>=0;j--){
			System.out.print(arr[j] + " ");
		}		

	
	}	

		
}

