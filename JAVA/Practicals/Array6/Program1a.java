

import java.util.*;

class Demo1a{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int flag=0;
		for(int j=0;j<size-1;j++){
			if(arr[j]>arr[j+1]){
					flag++;
				}
				else{
					flag=0;
				}
			}
		
		if(flag>0){
			System.out.print("Array is in descending order");
		}
		else{
			System.out.print("Array not in descending order");
		}
		
	}
}

