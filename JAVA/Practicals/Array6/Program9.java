

import java.util.*;

class Demo9{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array1: ");
		int size=sc.nextInt();
		int arr[] = new int[size];
		
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int count=0;
		for(int j=0;j<size;j++){
			int rev=0;

			int num=arr[j];
			while(arr[j]>0){
				int rem=arr[j]%10;
				rev = rem+rev*10;
				arr[j]=arr[j]/10;
			}if(num==rev){
				count++;
			}
		}
		System.out.print("Count: " + count);

	
	}	

		
}

