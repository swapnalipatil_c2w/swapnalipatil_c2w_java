

import java.util.*;

class Demo6{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of array1: ");
		int size=sc.nextInt();
		int arr[] = new int[size];
		
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter number: ");
		int num=sc.nextInt();
		for(int j=0;j<size;j++){
			if(arr[j]%num==0){
				System.out.println("multiple of " +num + " found at " + j );
			}
		}		

	
	}	

		
}

