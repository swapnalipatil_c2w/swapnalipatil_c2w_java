

import java.io.*;

class Demo5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter number: ");
		int num= Integer.parseInt(br.readLine());
		int var=0;
		int temp=num;
		while(num>0){
			int rem=num%10;
			num=num/10;
			var=var*10+rem;
		}
		if(var==temp){
			System.out.println(temp + " is a palindrome number");
		}
		else{
			System.out.println(temp + " is not a palindrome number");
		}
	}
}
