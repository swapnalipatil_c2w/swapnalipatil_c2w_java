

import java.io.*;

class Demo1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());
		int temp=1;
		int count=0;
		while(temp<=num){
			if(num%temp==0){
				count++;
				System.out.print(temp);
			}
			temp++;
		}
		if(count==2){
			System.out.println("Prime number");
		}
		else{
			System.out.println("Not prime");
		}
	}
}
