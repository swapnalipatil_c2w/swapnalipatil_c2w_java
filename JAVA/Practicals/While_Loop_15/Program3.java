

import java.io.*;

class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());
		int fact = 1;
		int temp=num;
		while(num>=1){
			fact=fact*num;
			num--;
		}
		System.out.print("Factorial of "+ temp + " is " + fact);
		System.out.println();
	}
}

