

import java.util.*;

class Demo4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		
		for(int i=0;i<rows;i++){
			for(int sp=1;sp<=i;sp++){
				System.out.print(" "+ "\t");
			}
			int ch=65+i;
			for(int j=rows;j>i;j--){
				System.out.print((char)(ch) + "\t");
				ch++;
				
			}
			

			System.out.println();
		}
	}
}

