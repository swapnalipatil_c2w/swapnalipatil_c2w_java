

import java.util.*;

class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows= sc.nextInt();
		int num=0;
		for(int i=1;i<=rows;i++){
			for(int sp=rows;sp>i;sp--){
				System.out.print(" ");
			}
			int num2=num+1;
			for(int j=1;j<=i*2-1;j++){
					if(j<i){
						System.out.print(num2--);
						
					}
					else{
						System.out.print(num2++);
						
					}
			}
			num++;
			
			
			System.out.println();
		}
	}
}
