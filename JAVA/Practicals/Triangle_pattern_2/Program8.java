

import java.io.*;

class Demo8{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		int ch = 99;
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=i;j++){
				if(j%2==1){
					System.out.print(j + " ");
				}
				else{
					System.out.print((char)(ch)+ " ");
					ch+=2;
				}
			}
			System.out.println();
		}
	}
}
				

