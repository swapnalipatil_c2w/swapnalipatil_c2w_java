import java.util.*;

class Demo9{

	public static void main(String[] args){
	
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row = sc.nextInt();

		for(int i=1;i<=row;i++){

			for(int j=1;j<=row;j++){
			
				if(i%2==1&& j%2==1 || i%2==0&& j%2==0 ){
				
					
						System.out.print(j*2+" ");
					}else{
					
						System.out.print(j*3+" ");
					}
				
			}

			System.out.println();
		}
	}
}


