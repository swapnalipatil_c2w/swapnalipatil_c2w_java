
import java.io.*;

class Demo1{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		
		int num=rows;
		for(int i=1;i<=rows;i++){
			int ch = 64+rows;
			for(int j=1;j<=rows;j++){
				if(i%2==1){
					System.out.print((char)(ch) + " ");
				}
				else{
					System.out.print(num + " ");
				}
				ch--;
			}
			System.out.println();
		}
	}
}

