
import java.io.*;

class Demo7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		
		int num=rows;
		char ch = 'A';
		for(int i=1;i<=rows;i++){
			for(int j=1;j<=rows;j++){
				if(i%2==1){
					if(j%2==1){
						System.out.print(ch + " ");
					}
					else{
						System.out.print(num + " ");
					}
				}
				else{
					if(j%2==1){
						System.out.print(num + " ");
					}
					else{
						System.out.print(ch + " ");
					}
				}
				num++;


				

			}
			ch++;
			System.out.println();
		}
	}
}

