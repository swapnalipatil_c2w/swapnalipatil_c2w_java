

class Demo6{
	public static void main(String[] args){
		int age=-18;
		if(age>=18){
			System.out.println(age+" age is eligible for voting");
		}
		else if(age<18 && age>=1){
			System.out.println(age+" age is not eligible for voting");
		}
		else{
			System.out.println("Invalid age");
		}
	}
}
