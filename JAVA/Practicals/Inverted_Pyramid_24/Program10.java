

import java.util.*;

class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows=sc.nextInt();
		
		for(int i=1;i<=rows;i++){
			for(int sp=1;sp<i;sp++){
				System.out.print("\t");
			}
			int num=rows+1-i;
			for(int j=1;j<=(rows-i)*2+1;j++){
				if(j<rows-i+1){
					System.out.print(num-- + "\t");
				}
				else{
					System.out.print(num++ + "\t");
				}
				
		
			}
			
			System.out.println();
		}
	}
}

