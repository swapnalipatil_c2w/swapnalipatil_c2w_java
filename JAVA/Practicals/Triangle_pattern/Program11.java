

import java.io.*;

class Demo11{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		if(rows%2==0){
			for(int i=1;i<=rows;i++){
			int ch=i+64;
			for(int j=rows;j>=i;j--){
				if(ch%2==1){
					System.out.print((ch)+ " ");
					
				}
				else{
					System.out.print((char)(ch)+ " ");
				}
				ch++;
			}
			System.out.println();
		}
		}
		else{
			for(int i=1;i<=rows;i++){
				int ch=i+64;
				for(int j=rows;j>=i;j--){
					if(ch%2==1){
						System.out.print((char)(ch)+ " ");
					}
					else{
						System.out.print(ch + " ");
					}
					ch++;
				}
				System.out.println();
			}
		}
	
	}
}
