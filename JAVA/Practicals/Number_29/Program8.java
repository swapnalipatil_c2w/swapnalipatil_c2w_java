

import java.util.*;

class Demo8{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num: ");
		int num=sc.nextInt();
		int flag=0;
		while(num>0){
			int rem=num%10;
			num=num/10;
			if(rem == 0){
				
				flag++;
				break;
			}
			else{
				
				flag=0;
			}
		}
		if(flag>0){
			System.out.println("Its a duck number");
		}
		else{
			System.out.println("Not a duck number");
		}
	}
}
