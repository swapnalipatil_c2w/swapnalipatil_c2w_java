

import java.util.*;

class Demo5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num: ");
		int num=sc.nextInt();
		int digit=0;
		int var =0;
		int temp=num;
		while(num>0){
			num=num/10;
			digit++;
		}
		int sq=temp*temp;
		while(digit>0){
			int rem=sq%10;
			var = rem+var*10;
			sq=sq/10;
			digit--;
		}
		
		int rev=0;
		while(var>0){
			int rem2=var%10;
			var=var/10;
			rev=rem2+rev*10;
		}
		if(rev==temp){
			System.out.println("Automorphic number");
		}
		else{
			System.out.println("Not automorphic");
		}
	}
}
