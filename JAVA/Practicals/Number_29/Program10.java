

import java.util.*;

class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number: ");
		int num = sc.nextInt();
		int temp=num;
		int sum=0;
		
		while(num>0){
			int cube=0;
			int rem=num%10;
			cube=rem*rem*rem;
			sum=sum+cube;
			num=num/10;
		}
		if(sum==temp){
			System.out.println("Its a armstrong number");
		}
		else{
			System.out.println("Not a armstrong number");
		}
	}
}
