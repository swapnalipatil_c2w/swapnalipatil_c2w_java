

import java.util.*;

class Demo7{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num: ");
		int num=sc.nextInt();
		int sum=0;
		int sq=0;
		while(num>0){
			int rem=num%10;
			sq=rem*rem;
			sum=sum+sq;
			num=num/10;
		}
		if(sum==1){
			System.out.println("Its a happy number");
		}
		else{
			System.out.println("Not a happy number");
		}
	}
}
