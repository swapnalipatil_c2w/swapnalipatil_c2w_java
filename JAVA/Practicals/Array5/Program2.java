

import java.util.*;

class Demo2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size = sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int esum =0;
		int osum=0;
		for(int j=0;j<size;j++){
			if(arr[j]%2==0){
				esum = esum+arr[j];
			}
			else{
				osum = osum+arr[j];
			}

		}
		System.out.println("Odd sum : "+ osum);
		System.out.println("Even sum : "+ esum);	
		
		
	}
}
