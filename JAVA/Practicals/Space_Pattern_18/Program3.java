

import java.io.*;

class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			for(int space=1;space<=rows-i;space++){
				System.out.print(" "+ "\t");
			}
			int ch=64+rows;
			for(int j=1;j<=i;j++){
				System.out.print((char)(ch) + "\t");
				ch--;
			}
			System.out.println();
		}
	}
}
