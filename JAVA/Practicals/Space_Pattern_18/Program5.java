

import java.io.*;

class Demo5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		int num=1;
		for(int i=1;i<=rows;i++){
			for(int space=1;space<=rows-i;space++){
				System.out.print(" " + "\t");
			}
			
			for(int j=1;j<=i;j++){
				System.out.print(num + "\t");
				num++;
			}
			System.out.println();
		}
	}
}

