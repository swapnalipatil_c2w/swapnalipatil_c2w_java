

import java.util.*;

class Demo3{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size: ");
		int size=sc.nextInt();
		int arr[]= new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		System.out.print("Enter number: ");
		int num=sc.nextInt();
		int cnt=0;
		for(int j=0;j<size;j++){
			if(arr[j]==num){
				cnt++;
			}
		}
		System.out.print(cnt + " ");
	}
}
