

import java.util.*;

class Demo4{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		int col=0;
		int num = rows;

		for(int i=1;i<=rows*2-1;i++){
			if(i<=rows){
				col=i;
			}
			else{
				col--;
				num=num+2;
			}
			for(int j=1;j<=col;j++){
				System.out.print(num + " ");
			}
			System.out.println();
			num--;
		}
		System.out.println();
	}
}
