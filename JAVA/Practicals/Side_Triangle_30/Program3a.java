import java.util.*;

class Demo3a {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter rows: ");
        int rows = sc.nextInt();
        int col = 0;

        for (int i = 1; i < rows * 2; i++) {
            if (i <= rows) {
                col = i;
            } else {
                col--;
            }
            int num = i;
            int num2 = rows - i;

            // Printing spaces for the upper half of the pattern
            for (int j = 1; j <= rows - col; j++) {
                System.out.print("  ");
            }

            for (int j = 1; j <= col; j++) {
                if (i <= rows) {
                    System.out.print(num-- + "  ");
                } else if (i > rows) {
                    System.out.print(num2-- + "  ");
                }
            }
            System.out.println();
        }
    }
}

