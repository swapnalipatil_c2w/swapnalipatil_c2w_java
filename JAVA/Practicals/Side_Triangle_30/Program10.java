

import java.util.*;

class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		int num1=64+rows;
		int num2=1;
		for(int i=1;i<rows*2;i++){
			for(int sp=num1;sp>1;sp--){
				System.out.print(" ");
			}
			int num3=num1;
			for(int j=1;j<=num2;j++){
				System.out.print((char)num3++);
			}
			if(i<rows){
				
				num1--;
				num2++;
			}
			else{
				
				num1++;
				num2--;
			}
			System.out.println();
		}
	}
}
