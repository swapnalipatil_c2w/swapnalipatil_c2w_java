

import java.util.*;

class Demo2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		int col=0;
		for(int i=1;i<rows*2;i++){
			if(i<=rows){
				col=i;
			}
			else{
				col--;
			}
			int num=1;		
			for(int j=1;j<=col;j++){
				System.out.print(num++ + "\t");
			}
			System.out.println();
		}
	}
}
