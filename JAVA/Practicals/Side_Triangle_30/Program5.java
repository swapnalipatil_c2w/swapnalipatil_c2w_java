

import java.util.*;

class Demo5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int rows = sc.nextInt();
		int col=0;
		int ch = 64+rows;

		for(int i=1;i<=rows*2-1;i++){
			if(i<=rows){
				col=i;
			}
			else{
				col--;
				ch=ch+2;
			}
			for(int j=1;j<=col;j++){
				System.out.print((char)ch + " ");
			}
			System.out.println();
			ch--;
		}
		System.out.println();
	}
}
