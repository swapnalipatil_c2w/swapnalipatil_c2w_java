

import java.io.*;

class Demo6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		char arr[] = new char[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= (char)br.read();
			br.skip(1);
		}
		for(int j=0;j<arr.length;j++){	
			System.out.print(arr[j]);
			

		}
	}
}
