

import java.util.*;

class Demo2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter array size: ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		for(int j=0; j<arr.length;j++){
			System.out.print(arr[j] + " , ");
		}
	}
}
