

import java.io.*;

class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter array size: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}
		for(int j=0;j<arr.length;j++){
			if(j%2==1){
				System.out.println(arr[j] + " is an odd indexed element");
			}
			

		}
	}
}
