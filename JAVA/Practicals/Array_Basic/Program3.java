

import java.io.*;

class Demo3{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}
		for(int j=0;j<arr.length;j++){
			if(arr[j]%2==0){
				System.out.print(arr[j] + ", ");
			}

		}
	}
}
