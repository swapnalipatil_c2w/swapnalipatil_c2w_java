

import java.io.*;

class Demo4{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		int sum=0;
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
			if(arr[i]%2==1){
				sum=sum+arr[i];
			}
			
		}
		System.out.print(sum);
		
	}
}
