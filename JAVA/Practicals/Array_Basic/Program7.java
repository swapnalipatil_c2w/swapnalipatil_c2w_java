

import java.io.*;

class Demo7{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter size of array: ");
		int size = Integer.parseInt(br.readLine());
		int arr[] = new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]= Integer.parseInt(br.readLine());
		}
		for(int j=0;j<arr.length;j++){
			if(arr[j]%4==0){
				System.out.println(arr[j] + " is divisible by 4 and its index is " + j);
			}

		}
	}
}
