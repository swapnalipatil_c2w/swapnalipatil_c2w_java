
import java.util.*;
class Demo10{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows: ");
		int row=sc.nextInt();
		int num=1;
		for(int i=1;i<=row;i++){
			num=i;
			for(int j=1;j<=row;j++){
				System.out.print(num++ + " ");
			}
			System.out.println();
		}
	}
}
