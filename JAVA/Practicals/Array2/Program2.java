

import java.util.*;

class Demo2{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size:");
		int size = sc.nextInt();
		int arr[]=new int[size];
		for(int i=0;i<size;i++){
			arr[i]=sc.nextInt();
		}
		int sum=0;
		for(int j=0;j<size;j++){
			if(arr[j]%3==0){
				sum=sum+arr[j];
				//System.out.println();
				System.out.print(arr[j] + " ");
			}
		}
		System.out.println("Count of even num is : "+ sum);
	}
}
