

class Demo12{
	public static void main(String[] args){
		int row=3;
		char ch='A';
		int num=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(num%2==1){
					System.out.print(num + " ");
				}
				else{
					System.out.print(ch + " ");
				}
				ch++;
				num++;
			}
			System.out.println();
		}
	}
}
