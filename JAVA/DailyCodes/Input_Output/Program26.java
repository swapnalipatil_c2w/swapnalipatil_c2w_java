

import java.io.*;

class Demo26{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Company name: ");
		String cmpName = br.readLine();
		System.out.print("Enter employee name: ");
		String empName = br.readLine();
		System.out.print("Enter emp Id: ");
		int empId = Integer.parseInt(br.readLine());
		System.out.print("Enter emp Salary: ");
		double sal = Double.parseDouble(br.readLine());
		System.out.println("Company Name: " + cmpName);
		System.out.println("Employee Name: "+ empName);
		System.out.println("Employee ID: "+ empId);
		System.out.println("Employee Salary: "+ sal);
	}
}
