

import java.io.*;

class Demo20{
	public static void main(String[] args)throws IOException{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String name = br.readLine();
		System.out.print("Name : "+ name);
		String compName = br.readLine();
		System.out.println("Company Name: " + compName);
	}
}
