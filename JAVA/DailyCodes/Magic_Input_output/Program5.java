

import java.util.*;

class Demo5{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter num : ");
		int num = sc.nextInt();
		if(num>=16 && num<=160){
			if(num%16==0){
				System.out.println(num + " is present in table of 16");
			}
			else{
				System.out.println(num + " is not present in table of 16");
			}
		}
		else if(num%16==0){
			System.out.println(num + " is divisible by 16 but not in table of 16");
		}
		else{
			System.out.println(num + " is not in table of 16");
		}
	}
}
