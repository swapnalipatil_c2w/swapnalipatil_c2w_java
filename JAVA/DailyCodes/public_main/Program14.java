

class Demo14{
	int x=10;
	static int y=20;
	void fun(){
		System.out.println("In fun function");
	}
	static void run(){
		System.out.println("In run method");
	}
	public static void main(String[] args){
		Demo14 obj = new Demo14();
		System.out.println(obj.x);
		System.out.println(obj.y);
		obj.fun();
		obj.run();
	}
}
