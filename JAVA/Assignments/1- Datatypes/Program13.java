

class Demo13 {
    public static void main(String[] args) {
        
        float current = 5.0f; 
        float voltage = 12.0f; 

        
        System.out.println("Current: " + current + " amperes");
        System.out.println("Voltage: " + voltage + " volts");
    }
}
