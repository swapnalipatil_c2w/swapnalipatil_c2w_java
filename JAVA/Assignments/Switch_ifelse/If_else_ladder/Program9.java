

class Demo9{
	public static void main(String[] args){
		int sub1=35;
		int sub2=30;
		int sub3=35;
		int sub4=35;
		int sub5=35;

		if(sub1>=35 && sub2>=35 && sub3>=35 && sub4>=35 && sub5>=35){
			int sum=sub1+sub2+sub3+sub4+sub5;
			int grade=sum/5;
			if(grade>=90 && grade<=80){
				System.out.println("First class with Distinction");
			}
			else if(grade>=70 && grade<80){
				System.out.println("First class");
			}
			else if(grade>=60 && grade<=50){
				System.out.println("Second class");
			}
			else if(grade>=35 && grade<50){
				System.out.println("Pass");
			}
		}
		else{
			System.out.println("Fail");
		}
	}
}

