

class Demo2{
	public static void main(String[] args){
		char grade='E';
		switch(grade){
			case 'O':
				System.out.println("Outstanding");
				break;
			case 'A':
				System.out.println("Excellent");
				break;
			case 'B':
				System.out.println("Good");
				break;
			case 'C':
				System.out.println("Avegrage");
				break;
			case 'D':
				System.out.println("Pass");
				break;
			case 'F':
				System.out.println("Fail");
				break;
			default:
				System.out.println("Enter correct grade");
		}
	}
}
