

class Demo3{
	public static void main(String[] args){
		String str="XL";
		switch(str){
			case "S":
				System.out.println("Small");
				break;
			case "XS":
				System.out.println("Extra Small");
				break;
			case "L":
				System.out.println("Large");
				break;
			case "XL":
				System.out.println("Extra Large");
				break;
			case "XXL":
				System.out.println("Double XL");
				break;
			default:
				System.out.println("Enter correct size");
		}
	}
}

