

class Demo9 {
    public static void main(String[] args) {
        int sub1 = 95;
        int sub2 = 90;
        int sub3 = 90;
        int sub4 = 97;
        int sub5 = 95;

        if (sub1 >= 50 && sub2 >= 50 && sub3 >= 50 && sub4 >= 50 && sub5 >= 50) {
            int sum = sub1 + sub2 + sub3 + sub4 + sub5;
            

            switch (sum/5) {
                case 90:
                    System.out.println("First class with Distinction");
                    break;
                case 80:
                case 70:
                    System.out.println("First class");
                    break;
                case 60:
                    System.out.println("Second class");
                    break;
                default:
                    System.out.println("Pass");
                    break;
            }
        } else {
            System.out.println("Fail");
        }
    }
}

