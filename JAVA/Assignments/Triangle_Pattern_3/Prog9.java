import java.util.Scanner;

class Prog9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of rows: ");
        int numRows = scanner.nextInt();
        
        int startValue = numRows * 2 + 1+10;;
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numRows - i; j++) {
                System.out.print((startValue - 2 * j) + " ");
            }
            System.out.println();
            startValue -= 2;
        }
        scanner.close();
    }
}

