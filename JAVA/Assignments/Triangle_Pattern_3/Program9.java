

import java.io.*;
class Demo9{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Rows: ");
		int rows = Integer.parseInt(br.readLine());
		int num=rows*(rows+1)-1;
		for(int i=1;i<=rows;i++){
			for(int j=rows;j>=i;j--){
				System.out.print(num + " ");
				num=num-2;
			}
			System.out.println();
		}
	}
}
