

import java.io.*;

class Demo6{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int num=1;
			char ch='a';
			for(int j=rows;j>=i;j--){
				if(rows%2==0){
					if(j%2==0){
						System.out.print(num + " ");
						num++;
					}
					else{
						System.out.print(ch + " ");
						ch++;
					}
				}
				else{
					if(j%2==1){
						System.out.print(num + " ");
						num++;
					}
					else{
						System.out.print(ch + " ");
						ch++;
					}
				}

				
			}

			System.out.println();
		}
	}
}
				
