import java.io.*;

class Demo4 {

	public static void main(String[] args)throws IOException {

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.print("Rows = ");
		int rows = Integer.parseInt(br.readLine());

		int ch = 64 + (rows*(rows+1))/2;
		for(int i=1; i<=rows; i++){

			for(int j=rows; j>=i; j--){

				System.out.print((char)ch-- + " ");
			}
			System.out.println();
		}

	}
}


  
