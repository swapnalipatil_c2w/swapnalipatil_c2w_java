import java.io.*;

class Demo7a{

        public static void main(String[] args)throws IOException {

                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

                System.out.print("Rows = ");
                int rows = Integer.parseInt(br.readLine());

                for(int i=1; i<=rows; i++){

			int num1 = 1;
                        int num2 = 1+ rows-i;
                        int ch = 97 + rows-i;
			for(int j=rows; j>=i; j--){

                                if(num1%2 == 1)
                                        System.out.print(num2 + " ");
                                else
                                        System.out.print((char)ch + " ");
				num1++;
				num2--;
				ch--;
                        }
                        System.out.println();
                }
        }
}

