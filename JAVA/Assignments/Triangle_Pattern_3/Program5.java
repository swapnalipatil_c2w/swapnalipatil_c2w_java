

import java.io.*;

class Demo5{
	public static void main(String[] args)throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter Rows: ");
		int rows = Integer.parseInt(br.readLine());
		for(int i=1;i<=rows;i++){
			int ch='A';
			for(int j=rows;j>=i;j--){
				if(i%2==1){
					System.out.print((char)ch + " ");
				}
				else{
					System.out.print((char)(ch+32) + " ");
				}
				ch++;
			}
			System.out.println();
		}
	}
}

				
